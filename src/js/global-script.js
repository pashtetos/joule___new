$(document).ready(function () {

    $('.equipment-btn').on('click', function () {
        $(this).parent().toggleClass('active');
    });
    $(document).on('click', function (e) {
        var container = $(".equipment-dropdown");
        if (container.has(e.target).length === 0) {
            container.removeClass('active');
        }
    });
    $('.hamburger').on('click', function () {
        $('.header-button').toggleClass('active');
        $(this).toggleClass('close');
    });

    $('.show-more').on('click', function (e) {
        e.preventDefault();
        $('.review-company').next().slideDown(400).css('display', 'flex');
    });
    $('.read-more').on('click', function (e) {
        e.preventDefault();
        $(this).parent().find('.text-block').css('height', 'auto').addClass('full-text');
        $(this).hide();
    });

    // if ($('.review-company').hasClass('.review-btn')) {
    //     $(this).addClass('test');
    // }

    var swiper_completed_projects = new Swiper('.swiper-completed-projects', {
        loop: true,
        spaceBetween: 22,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        slidesPerView: 4,
        breakpoints: {
            1200: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            650: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            425: {
                slidesPerView: 1,
                spaceBetween: 30
            }
        }
    });

    var count_slides = ($('.swiper-completed-projects .swiper-slide').length);
    if ($('.swiper-completed-projects .swiper-slide').length < 4) {
        $('.swiper-completed-projects .swiper-slide').addClass('swiper-no-swiping');
        $('.swiper-completed-projects').addClass('swiper-disabled');


        var swiper_completed_projects = new Swiper('.swiper-completed-projects', {
            // disableOnInteraction: false,
            // slidesPerView: count_slides
            slidesPerView: 'auto',
            noSwipingClass: 'swiper-no-swiping'
        });

    }
    $('.popup-link').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true,
        }
    });


    var number_companies = $('.group-company ul li').length;
    $('.group-company ul li').slice(0, 21).show();

    $('.view-more').on('click', function (e) {
        e.preventDefault();
        $(this).parent().find('ul li').slice(0, number_companies).slideDown(400);
        $(this).hide();
    });

    console.log($('.service-partners ul li').length);

    if ($('.service-partners ul li').length < 5) {
        $('.service-partners').find('.view-more').hide();
    }

    if ($(window).width() < 993) {
        console.log('hello');
        $('.service-partners ul li').slice(0, 3).show();
        $('.dealers ul li').slice(0, 3).show();
        $('.service-partners ul li').slice(3, number_companies).hide();
        $('.dealers ul li').slice(3, number_companies).hide();

    }

    if ($(window).width() < 650) {
        if ($('.service-partners ul li').length < 5){
            $('.service-partners').find('.view-more').hide();
        }
        $('.service-partners ul li').slice(0, 8).show();
        $('.dealers ul li').slice(0, 8).show();
    }

    $('.select2').select2()


});




